/*
Navicat MySQL Data Transfer
Source Host     : localhost:3306
Source Database : project1
Target Host     : localhost:3306
Target Database : project1
Date: 2015-07-22 10:16:04
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `ad_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `ad_name` varchar(10) NOT NULL,
  `ad_pwd` char(32) NOT NULL,
  PRIMARY KEY (`ad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('6', 'admin1', 'e10adc3949ba59abbe56e057f20f883e');

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `site_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `site_title` varchar(10) NOT NULL,
  `site_name` varchar(10) NOT NULL,
  `site_value` text NOT NULL,
  `site_type` varchar(8) NOT NULL,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('1', '网站标题', 'title', '娱乐网站', 'text');
INSERT INTO `config` VALUES ('2', '网站描述', 'desc', '一个娱乐网站', 'textarea');
INSERT INTO `config` VALUES ('3', '网站关键字', 'keywords', '娱乐，娱乐网站，娱乐新闻', 'textarea');
INSERT INTO `config` VALUES ('4', '网站域名', 'domain', 'www.yule.com', 'text');
INSERT INTO `config` VALUES ('5', '公司地址', 'addr', '南京市江宁区将军大道128号', 'textarea');
INSERT INTO `config` VALUES ('6', '公司电话', 'tel', '025-87654321', 'text');
INSERT INTO `config` VALUES ('21', 'test1', 'testtest', 'hahhahahah111', 'text');
INSERT INTO `config` VALUES ('24', 'test2', 'ttt', 'jdslfja;dj', 'text');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `nid` smallint(5) NOT NULL AUTO_INCREMENT,
  `n_c_id` smallint(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `pubtime` bigint(11) NOT NULL,
  `updatetime` bigint(11) DEFAULT NULL,
  `pubadmin` varchar(20) NOT NULL,
  `updateadmin` varchar(20) DEFAULT NULL,
  `content` text NOT NULL,
  `ifshow` tinyint(1) DEFAULT '1',
  `keywords` varchar(200) DEFAULT NULL,
  `num_click` bigint(11) DEFAULT '0',
  `timgs` varchar(255) DEFAULT NULL,
  `pimgs` varchar(255) DEFAULT NULL,
  `timg_titles` varchar(255) DEFAULT NULL,
  `pimg_titles` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`nid`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('4', '3', 'test4', '1437486487', '14156478378', 'admin1', 'admin2', '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', './uploads/pimgs/1.jpg;./uploads/pimgs/7ccc8aca7bcb0a46d4eef1196963f6246a60af85.jpg', '', 'bb;cc');
INSERT INTO `news` VALUES ('5', '3', 'test4', '1437450387', '14156478378', 'admin1', 'admin2', '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', '', '', '');
INSERT INTO `news` VALUES ('6', '3', 'test4', '1437450387', '14156478378', 'admin1', 'admin1', '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', '', '', '');
INSERT INTO `news` VALUES ('7', '3', 'test4', '1437450387', '14156478378', 'admin1', 'admin1', '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', '', '', '');
INSERT INTO `news` VALUES ('12', '3', 'test4', '1437450387', '14156478378', 'admin1', 'admin2', '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', '', '', '');
INSERT INTO `news` VALUES ('13', '3', 'test4', '1437450387', '14156478378', 'admin1', 'admin1', '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', '', '', '');
INSERT INTO `news` VALUES ('14', '3', 'test4', '1437450387', '14156478378', 'admin1', 'admin1', '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', '', '', '');
INSERT INTO `news` VALUES ('15', '3', 'test4', '1437450387', '14156478378', 'admin1', 'admin1', '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', '', '', '');
INSERT INTO `news` VALUES ('16', '3', 'test4', '1437450387', '14156478378', 'admin1', 'admin1', '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', '', '', '');
INSERT INTO `news` VALUES ('17', '3', 'test4', '1437450387', '14156478378', 'admin1', 'admin2', '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', '', '', '');
INSERT INTO `news` VALUES ('18', '3', 'test4', '1437450387', null, 'admin1', null, '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', '', '', '');
INSERT INTO `news` VALUES ('19', '3', 'test4', '1437450387', null, 'admin1', null, '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', '', '', '');
INSERT INTO `news` VALUES ('20', '3', 'test4', '1437450387', null, 'admin1', null, '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', '', '', '');
INSERT INTO `news` VALUES ('21', '3', 'test4', '1437450387', null, 'admin1', null, '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', '', '', '');
INSERT INTO `news` VALUES ('22', '3', 'test4', '1437450387', null, 'admin1', null, '<p>aaaaaaaaaaaaaaa</p>', '1', '', '0', '', '', '', '');

-- ----------------------------
-- Table structure for news_cate
-- ----------------------------
DROP TABLE IF EXISTS `news_cate`;
CREATE TABLE `news_cate` (
  `cid` smallint(5) NOT NULL AUTO_INCREMENT,
  `cname` varchar(20) NOT NULL,
  `fid` smallint(5) NOT NULL,
  `level` tinyint(3) NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news_cate
-- ----------------------------
INSERT INTO `news_cate` VALUES ('1', '网站帮助', '0', '1');
INSERT INTO `news_cate` VALUES ('2', '公司动态', '0', '1');
INSERT INTO `news_cate` VALUES ('3', '公司技术动态', '2', '2');
INSERT INTO `news_cate` VALUES ('4', '国内新闻', '0', '1');
INSERT INTO `news_cate` VALUES ('5', '公司活动', '2', '2');
INSERT INTO `news_cate` VALUES ('6', '南京新闻', '4', '2');
INSERT INTO `news_cate` VALUES ('7', '鼓楼区新闻', '6', '3');
INSERT INTO `news_cate` VALUES ('8', '上海新闻', '4', '2');
INSERT INTO `news_cate` VALUES ('9', '关于网站', '1', '2');
INSERT INTO `news_cate` VALUES ('10', '支付方式', '1', '2');
INSERT INTO `news_cate` VALUES ('11', '关于我们', '1', '2');
INSERT INTO `news_cate` VALUES ('20', 'fff', '8', '3');
INSERT INTO `news_cate` VALUES ('24', 'test', '20', '4');

-- ----------------------------
-- Table structure for product_cate
-- ----------------------------
DROP TABLE IF EXISTS `product_cate`;
CREATE TABLE `product_cate` (
  `cid` smallint(5) NOT NULL AUTO_INCREMENT,
  `cname` varchar(20) NOT NULL,
  `fid` smallint(5) NOT NULL,
  `level` tinyint(3) NOT NULL,
  `ifshow` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_cate
-- ----------------------------
INSERT INTO `product_cate` VALUES ('1', '照相机', '0', '1', '1');
INSERT INTO `product_cate` VALUES ('4', '相机镜头', '1', '2', '1');
INSERT INTO `product_cate` VALUES ('5', '长焦镜头', '4', '3', '0');
INSERT INTO `product_cate` VALUES ('6', '手机', '0', '1', '1');
INSERT INTO `product_cate` VALUES ('7', '手机配件', '6', '2', '1');
INSERT INTO `product_cate` VALUES ('8', 'iPhone', '6', '2', '1');
INSERT INTO `product_cate` VALUES ('11', '单反', '1', '2', '1');
INSERT INTO `product_cate` VALUES ('12', '胶片相机', '1', '2', '1');
INSERT INTO `product_cate` VALUES ('13', 'iPhone5', '8', '3', '1');
INSERT INTO `product_cate` VALUES ('14', 'iPhone6', '8', '3', '1');
INSERT INTO `product_cate` VALUES ('15', '笔记本电脑', '0', '1', '1');
INSERT INTO `product_cate` VALUES ('20', '安卓手机', '6', '2', '1');
INSERT INTO `product_cate` VALUES ('22', '小米', '20', '3', '1');
INSERT INTO `product_cate` VALUES ('24', '联想笔记本', '15', '2', '1');
INSERT INTO `product_cate` VALUES ('25', '惠普笔记本', '15', '2', '1');
INSERT INTO `product_cate` VALUES ('27', '55-180mm镜头', '5', '4', '1');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `pid` smallint(5) NOT NULL AUTO_INCREMENT,
  `p_c_id` smallint(5) NOT NULL,
  `pname` varchar(200) NOT NULL,
  `price` float(10,2) NOT NULL,
  `timg_titles` varchar(255) DEFAULT NULL,
  `timgs` varchar(255) DEFAULT NULL,
  `descp` text,
  `pubtime` bigint(11) NOT NULL,
  `pubadmin` varchar(20) NOT NULL,
  `addr` varchar(200) NOT NULL,
  `color` varchar(100) NOT NULL,
  `size` varchar(100) DEFAULT NULL,
  `ifshow` tinyint(2) DEFAULT '1',
  `sprice` float(10,2) DEFAULT NULL,
  `keywords` varchar(200) DEFAULT NULL,
  `num_stock` bigint(11) DEFAULT '0',
  `series` varchar(20) DEFAULT NULL,
  `pimg_titles` varchar(255) DEFAULT NULL,
  `pimgs` varchar(255) DEFAULT NULL,
  `num_click` bigint(11) DEFAULT '0',
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', '20', 'MOTOROLA里程碑', '2000.00', null, null, null, '1436921611', 'admin1', '南京', 'black', null, '1', null, null, null, null, null, null, '0');
INSERT INTO `products` VALUES ('2', '20', '小米', '2500.00', null, null, null, '1436921611', 'admin1', 'nanjing', 'white', null, '1', null, null, null, null, null, null, '0');
INSERT INTO `products` VALUES ('3', '20', '红米', '800.00', null, null, null, '1436921611', 'admin1', 'beijing', 'white', null, '1', null, null, null, null, null, null, '0');
INSERT INTO `products` VALUES ('4', '22', '小米2', '1500.00', '', '', '<p>小米系列产品</p>', '1437116061', 'admin1', '北京', 'black;white;;;', ';;', '1', '111.00', 'bbb', '123123', '', '', '', '1');
INSERT INTO `products` VALUES ('8', '22', 'name', '876.12', null, '', 'dfasf', '1436952510', 'admin1', 'nanjing', 'red;black', 'xs;s;m', '1', '2000.12', 'jdsfkaj', '2000', 'dsfadsfadsf12312', null, '', '523');
INSERT INTO `products` VALUES ('9', '22', '小米3', '1000.00', '', '', '<p>哈哈哈哈哈哈哈哈</p>', '1437040723', 'admin1', '南京', 'black;white;yellow;green;silver;;;', '4.3吋;5.2吋;;;', '1', '123.00', 'asdfasdf', '12', 'JXIFJ84DJ987F', '', '', '123');
INSERT INTO `products` VALUES ('11', '27', 'adf', '12.00', null, '', '', '1437008963', 'admin1', 'sdf', 'black', '', '1', '123.00', 'sdf', '12', '', null, '', '133');
INSERT INTO `products` VALUES ('20', '22', '小米3', '1999.00', 'aa;bb;;;;', './uploads/timgs/1.jpg;./uploads/timgs/7ccc8aca7bcb0a46d4eef1196963f6246a60af85.jpg', '', '1437139609', 'admin1', '北京', 'black;white;;;', ';;', '1', null, '', null, '', 'cc;dd;;;;', './uploads/pimgs/5059124154491edd4460a1eb4bbee648.jpg;./uploads/pimgs/rainy-weather-hd-wallpaper.jpg', '5');
INSERT INTO `products` VALUES ('22', '7', 'test22', '12344.00', '', '', '', '1437123448', 'admin1', 'nanjing', 'black;white;;;', 'medium;big;;;', '1', '123.00', 'dsf', '11', '', '', '', '123123');
INSERT INTO `products` VALUES ('23', '22', '呵呵呵', '889.12', 'rocket;mountain;;;;', './uploads/timgs/60070e43fbf2b211d512e06ec88065380cd78e95.jpg;./uploads/timgs/7ccc8aca7bcb0a46d4eef1196963f6246a60af85.jpg', '', '1437315018', 'admin1', '南京', 'black;white;;;', ';;', '1', null, '', '0', 'ASDFSF2341F', 'grassland;;;;', './uploads/pimgs/5059124154491edd4460a1eb4bbee648.jpg', '0');
INSERT INTO `products` VALUES ('24', '27', 'sdfd', '13.00', ';;;', '', '', '1437395468', 'admin1', 'sdfs', 'black;white;;;', ';;', '1', null, '', '0', '', ';;;', '', '0');
INSERT INTO `products` VALUES ('25', '12', 'asdfad', '231.00', ';;;', '', '', '1437395476', 'admin1', 'asdf', 'black;white;;;', ';;', '1', null, '', '0', '', ';;;', '', '0');
INSERT INTO `products` VALUES ('26', '27', 'sdf', '123.00', ';;;', '', '', '1437395498', 'admin1', 'beijing', 'black;white;;;', ';;', '1', null, '', '0', '', ';;;', '', '0');
INSERT INTO `products` VALUES ('27', '22', 'sklfjewjf', '567.00', ';;;', '', '', '1437395515', 'admin1', 'beijing', 'black;white;;;', ';;', '1', null, '', '0', '', ';;;', '', '0');
INSERT INTO `products` VALUES ('28', '24', 'fsdfjkl', '123.00', ';;;', '', '', '1437395527', 'admin1', 'nanjing', 'black;white;;;', ';;', '1', null, '', '0', '', ';;;', '', '0');

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `nid` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) DEFAULT NULL,
  PRIMARY KEY (`nid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of test
-- ----------------------------
INSERT INTO `test` VALUES ('1', '10');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `uid` int(5) NOT NULL AUTO_INCREMENT,
  `uaccount` varchar(20) NOT NULL,
  `pwd` char(32) NOT NULL,
  `uname` varchar(20) NOT NULL,
  `sex` varchar(8) DEFAULT '保密',
  `email` varchar(60) NOT NULL,
  `lastlogin` bigint(11) DEFAULT NULL,
  `if_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
