<?php
	include_once("global.php");
	if (!isset($_SESSION['admin'])){
		msg("请先登录","index.php"); die;
	}
	if (isset($_POST['sub'])){
		if (!isset($_POST['catename'])||$_POST['catename']==''){
			msg('请填写分类名称',"news_cate_add.php");
			die;
		}
		$cname = trim($_POST['catename']);
		$fid = $_POST['fid'];
		$sql = "select level from news_cate where cid=$fid";
		$re = mysql_query($sql);
		$rec = mysql_fetch_assoc($re);
		$level = $rec['level']+1;
		
		// 判断是否具有相同名称和父分类的记录，如果有，则不允许添加
		$sql = "select * from news_cate where cname='$cname' and fid=$fid";
		$re = mysql_query($sql);
		if (mysql_num_rows($re)>0){
			msg('父分类下已有此分类',"news_cate_list.php"); die;
		}
		$sql = "insert into news_cate (cname,fid,level) value ('$cname',$fid,$level)";
		mysql_query($sql);
		if (mysql_affected_rows()<1){
			msg("添加失败","news_cate_add.php");
		}else{
			msg("添加成功","news_cate_list.php");
		}
	}
?>
<!doctype html>
<html>
	<head>
		<title> New Document </title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<link rel="stylesheet" href="./skin/css/base.css" />
		<link rel="stylesheet" href="./skin/css/main.css" />
		<link rel="stylesheet" href="./skin/css/main1.css" />
	</head>

	<body>
		<table class="outer_tab">
			<tr>
				<td class="title" id="tabtitle" colspan=2><span><img src='skin/images/frame/arr3.gif'>添加新闻分类：</span></td>
			</tr>
			<tr class="tr2">
				<td>
		<table class="inner_tab">
			<form method="post" action="#">
				<tr>
					<td class='tdleft'><span class="tdtitle">分类名称：</span></td>
					<td><input type="text" name="catename" /></td>
				</tr>
				<tr>
					<td><span class="tdtitle">上级分类：</span></td>
					<td>
						<select name='fid'>
							<option value="0" selected/>顶级分类</option>
							<?php 
								$sql = "select * from news_cate";
								$re = mysql_query($sql);
								while ($rec = mysql_fetch_assoc($re)){
									$arr[] = $rec;
								}
								function get_cates($arr,$value=0){
									static $classes = array();
									foreach ($arr as $key=>$rec){
										if ($rec['fid']==$value){
											$classes[] = $rec;
											get_cates($arr,$rec['cid']);
										}
									}
									return $classes;
								}
								$sorted_arr = get_cates($arr);
								foreach ($sorted_arr as $rec){
							?>
							<option value="<?=$rec['cid']?>" <?php 
								if (isset($_GET['cid'])&&($_GET['cid']==$rec['cid']))
									echo "selected";
							?>/>
								<?=str_repeat("----",$rec['level']-1).$rec['cname']?>
							</option>
							<?php }?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan=2 class="operation">
						<input type="submit" class="coolbt2" name="sub" value="添加">
					</td>
				</tr>
			</form>
		</table>
				</td>
			</tr>
		</table>
	</body>
</html>
