<?php
	include_once("global.php");
	if (!isset($_SESSION['admin'])){
		msg("请先登录","index.php"); die;
	}
	if (isset($_GET['sid'])){
		$sid = $_GET['sid'];
		$sql = "select * from config where site_id=$sid";
		$re = mysql_query($sql);
		if (mysql_num_rows($re)==1){
			$record = mysql_fetch_assoc($re);
		}else{
			msg("从数据库获取数据失败","conf.php"); die;
		}
	}
	if(isset($_POST['sub'])){
		$ctitle = trim($_POST['ctitle']);
		$cname = trim($_POST['cname']);
		$cdata = trim($_POST['cdata']);
		$ctype = $_POST['ctype'];
		if (isset($_GET['sid'])){
			$page = "conf.php";
		}else{
			$page = "conf_add_edit.php";
		}
		if ($ctitle=='' || $cname=='' || $cdata=='' || $ctype==''){
			msg('请填写完整的信息',$page);
		}
		if (!isset($_GET['sid'])){
			$sql = "select * from config where site_title='$ctitle'";
			$re = mysql_query($sql);
			if (mysql_num_rows($re)>0){
				msg("该标题已经存在，请重新配置","conf_add_edit.php"); die;
			}
			$sql = "select * from config where site_name='$cname'";
			$re = mysql_query($sql);
			if (mysql_num_rows($re)>0){
				msg("该名称已经存在，请重新配置","conf_add_edit.php"); die;
			}

			$sql = "insert into config (site_title, site_name, site_value, site_type) value ('$ctitle', '$cname', '$cdata', '$ctype')";
			mysql_query($sql);
			if (mysql_affected_rows()==1){
				msg("添加成功","conf.php"); die;
			}else{
				msg("添加失败，请重试","conf_add_edit.php"); die;
			}
		}else{
			
			$sql = "select * from config where site_title='$ctitle'";
			$re = mysql_query($sql);
			if (mysql_num_rows($re)>0){
				msg("该标题已经存在，请重新配置","conf.php"); die;
			}
			$sql = "select * from config where site_name='$cname'";
			$re = mysql_query($sql);
			if (mysql_num_rows($re)>0){
				msg("该名称已经存在，请重新配置","conf.php"); die;
			}
			$sid = $_GET['sid'];
			$sql = "update config set site_title='$ctitle', site_name='$cname', site_value='$cdata', site_type='$ctype' where site_id=$sid";
			echo $sql;
			mysql_query($sql);
			echo mysql_error();
			if (mysql_affected_rows()==1){
				msg("修改成功","conf.php"); die;
			}else{
				msg("修改失败，请重试","conf.php"); die;
			}
		}
	}

?>
<!doctype html>
<html>
  <head>
	 <title>添加/修改配置项</title>
	 <meta http-equiv="content-type" content="text/html;charset=utf-8"/> 
		<link rel="stylesheet" href="skin/css/base.css" />
		<link rel="stylesheet" href="skin/css/main.css" />
		<link rel="stylesheet" href="skin/css/main1.css" />
  </head>
  <body>
		<table class="outer_tab">
			<tr>
				<td colspan=2 class="title" id="tabtitle"><span><img src='skin/images/frame/arr3.gif'>添加配置项</span></td>
			</tr>
			<tr class="tr2">
			<td>
		<table class="inner_tab">
		<form action="#" method="post">
			<tr>
				<td class="tdleft"><span class="tdtitle">配置标题：</span></td>
				<td><input type="text" name="ctitle" value="<?=$record['site_title']?>" /></td>
			</tr>
			<tr>
				<td><span class="tdtitle">配置名称：</span></td>
				<td><input type="text" name="cname" value="<?=$record['site_name']?>" /></td>
			</tr>
			
			<tr>
				<td><span class="tdtitle">配置数据：</span></td>
				<td><input type="text" name="cdata" value="<?=$record['site_value']?>" /></td>
			</tr>
			
			<tr>
				<td><span class="tdtitle">配置类型：</span></td>
				<td>
					<input type="radio" name="ctype" value="text" 
					<?php
						if (($record['site_type']=="text") or !isset($_GET['sid'])){
							echo "checked";
						}
					?>
					>文本框
					<input type="radio" name="ctype" value="textarea"
					<?php
						if ($record['site_type']=="textarea"){
							echo "checked";
						}
					?>
					>文本域
				</td>
			</tr>
			
			<tr>
				<td colspan=2 class="operation">
					<input type="submit" class="coolbt2" name="sub" value="提交">
				</td>
			</tr>
		</form>
		</table>
		</td>
		</tr>
		</table>
 
  </body>
</html>
