<?php
	include_once("global.php");
	if (!isset($_SESSION['admin'])){
		msg("请先登录","index.php"); die;
	}
	if (!isset($_GET['cid'])){
		msg('请通过商品分类管理页面删除',"product_cate_list.php");
		die;
	}
	$cid = $_GET['cid'];
	$sql = "select * from product_cate where fid=$cid";
	$re = mysql_query($sql);
	if (mysql_fetch_assoc($re)>0){
		msg('该分类下有子分类或商品，无法删除','product_cate_list.php');
		die;
	}
	$sql = "delete from product_cate where cid=$cid";
	mysql_query($sql);
	if (mysql_affected_rows()<1){
		msg('删除失败','product_cate_list.php');
		die;
	}else{
		msg('删除成功','product_cate_list.php');
		die;
	}
?>
