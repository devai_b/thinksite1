<?php
	include_once("global.php");
	if (!isset($_SESSION['admin'])){
		msg("请先登录","index.php"); die;
	}
	if (isset($_GET['cid'])){
		$cid = $_GET['cid'];
		$sql = "select * from news_cate where cid = $cid";
		$re = mysql_query($sql);
		if (($rec = mysql_fetch_assoc($re))==false){
			msg('读取数据失败','news_cate_list.php');
			die;
		}
		$f = $rec['fid'];
		print_r($rec);
		$sql1 = "select * from news_cate where fid = $cid";
		$re1 = mysql_query($sql1);
		if (mysql_num_rows($re1)>0){
			$has_subcate = true;
		}else{
			$has_subcate = false;
		}
	}
	if (isset($_POST['sub'])&&isset($_GET['cid'])){
		if (!isset($_POST['cname'])||$_POST['cname']==''){
			msg("请填写分类名称","news_cate_edit.php");
			die;
		}
		$cid = $_GET['cid'];
		$cname = $_POST['cname'];
		$fid = $_POST['fid'];
		if ($fid!=0){
			$sql = "select level from news_cate where cid=$fid";
			$re = mysql_query($sql);
			if (mysql_num_rows($re)<1){
				msg("查找上级分类数据失败","news_cate_edit.php");
				die;
			}
			$rec = mysql_fetch_assoc($re);
			$level = $rec['level']+1;
		}else{
			$level = 1;
		}
		// 判断是否具有相同名称和父分类的记录，如果有，则不允许修改
		$sql = "select * from news_cate where cname='$cname' and fid=$fid";
		$re = mysql_query($sql);
		if (mysql_num_rows($re)>0){
			msg('父分类下已有此分类',"news_cate_list.php"); die;
		}
		$sql = "update news_cate set cname='$cname', fid=$fid, level=$level where cid=$cid";
		mysql_query($sql);
		if (mysql_affected_rows()<1){
			msg("修改失败","news_cate_list.php");
			die;
		}else{
			msg("修改成功","news_cate_list.php");
			die;
		}
	}
	
?>
<!doctype html>
<html>
	<head>
		<title> 修改分类 </title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<link rel="stylesheet" href="./skin/css/base.css" />
		<link rel="stylesheet" href="./skin/css/main.css" />
		<link rel="stylesheet" href="./skin/css/main1.css" />
	</head>

	<body>
		<table class="outer_tab">
			<tr>
				<td colspan=2 class="title" id="tabtitle"><span><img src='skin/images/frame/arr3.gif'>修改新闻分类：</span></td>
			</tr>
			<tr class="tr2">
				<td>
		<table class="inner_tab">
			<form method='post' action="#">
				<tr>
					<td class="tdleft"><span class="tdtitle">分类名称：</span></td>
					<td><input type="text" name="cname" value="<?=$rec['cname']?>"></td>
				</tr>
				<?php 
					if (!$has_subcate){
				?>
				<tr>
					<td><span class="tdtitle">上级分类：</span></td>
					<td>
						<select name='fid'>
							<option value="0">顶级分类</option>
							<?php 
								$sql = "select * from news_cate";
								$re = mysql_query($sql);
								while ($rec = mysql_fetch_assoc($re)){
									$arr[] = $rec;
								}
								function get_cates($arr,$value=0){
									static $classes = array();
									foreach ($arr as $key=>$rec){
										if ($rec['fid']==$value){
											$classes[] = $rec;
											get_cates($arr,$rec['cid']);
										}
									}
									return $classes;
								}
								$sorted_arr = get_cates($arr);
								foreach ($sorted_arr as $rec){
									if ($_GET['cid']!=$rec['cid']){
							?>
							<option value="<?=$rec['cid']?>" <?php 
								if ($f==$rec['cid'])
									echo "selected";
							?>>
								<?=str_repeat("----",$rec['level']-1).$rec['cname']?>
							</option>
							<?php }}?>
						</select>
					</td>
				</tr>
				<?php }else{?>
				<input type="hidden" name="fid" value="<?=$rec['fid']?>" />
				<?php }?>
				<tr>
					<td colspan=2 class="operation">
						<input type="submit" class="coolbt2" name="sub" value="修改" />
					</td>
				</tr>
			</form>
		</table>
				</td>
			</tr>
		</table>
	</body>
</html>
