<?php
	include_once("global.php");
	// 管理员是否登录
	if (!isset($_SESSION['admin'])){
		msg("请先登录","ad_login.php"); die;
	}
	// 颜色项目的条数
	if (!isset($_GET['num_color'])){
		$num_color = 3;
	}else{
		$num_color = $_GET['num_color'];
	}
	// 尺寸项目的条数
	if (!isset($_GET['num_size'])){
		$num_size = 3;
	}else{
		$num_size = $_GET['num_size'];
	}
	// 图片库项目的条数
	if (!isset($_GET['num_pimgs'])){
		$num_pimgs= 4;
	}else{
		$num_pimgs = $_GET['num_pimgs'];
	}
	if (isset($_POST['sub'])){
		$pubadmin = $_SESSION['admin'];
		$pname = $_POST['pname'];
		$price = $_POST['price'];
		$addr = $_POST['addr'];
		$p_c_id = $_POST['p_c_id'];
		$series = $_POST['series'];
		$descp = $_POST['descp'];
		$color = $_POST['color'];
		$size = $_POST['size'];
		$sprice = $_POST['sprice'];
		$keywords = $_POST['keywords'];
		$num_stock = $_POST['num_stock'];
		$num_click = $_POST['num_click'];
		$ifshow = $_POST['ifshow'];
		$timg_descp = $_POST['timg_descp'];
		$pimg_descp = $_POST['pimg_descp'];
		$arr_pimgs = $_POST['pimgs'];
		$arr_timgs = $_POST['timgs'];
		if (!isset($pname) || !isset($p_c_id) || !isset($price) || !isset($color) || !isset($addr) || $pname=='' || $p_c_id=='' || $price=='' || $color=='' || $addr==''){
			$toPage = "product_add.php";
			msg("带*的为必填内容，请填写完整",$toPage); die;
		}
		// 判断是否是在终极分类下添加
		$sql = "select * from product_cate where fid=$p_c_id";
		$re = mysql_query($sql);
		if (mysql_num_rows($re)>0){
			$toPage = "product_add.php";
			msg("该分类下还有子分类，只能在终极分类下添加商品",$toPage); die;
		}
	 
	// 图片上传函数	
	function _uploads($file_name, $types, $dir, $arr_imgs){
		if (!isset($_POST["sub"]))
			return "未通过post提交<br/>";

		$file = $_FILES[$file_name];
		$name = $file["name"];
		$error = $file['error'];
		$tmp_name = $file['tmp_name'];
		// echo ini_get("post_max_size")."<br/>";
		// echo ini_get("max_file_uploads")."<br/>";

		$str = implode("",$name);
		if ($str=="")
			return $arr_imgs;
		
		if (!is_dir($dir))
			mkdir($dir);

		for ($i=0; $i<count($name); $i++){
			if ($error[$i]!=0){
				echo "第".$i."个上传文件出错<br/>";
				continue;
			}

			$type = strtolower(end(explode(".",$name[$i])));


			if (!in_array($type, $types)){
				echo "第".$i."个文件类型不符合<br/>";
				continue;
			}

			if (!is_uploaded_file($tmp_name[$i])){
				echo "第".$i."个文件的临时文件不存在<br/>";
				continue;
			}
			
			if (substr($dir,-1)!='/')
				$dir .= '/';
			// $path = $dir.time()."_".rand(1,1000).".".$type;
			$path = $dir.$name[$i];
			// 图片重复判断（根据上传的文件名）
			if (is_array($arr_imgs)){
				if (in_array($path,$arr_imgs)){
					$msg = "第".($i+1)."个文件名重复，该图片可能已经上传";
					echo "<script>alert('$msg');</script>";
					continue;
			}}
			if (move_uploaded_file($tmp_name[$i], $path)){
				$arr_imgs[] = $path;
				echo "第".$i."个文件上传成功<br/>";
			}
			else
				echo "第".$i."个文件上传失败<br/>";
		}
		return $arr_imgs;
	}
	
	$types = array("jpg","jpeg","gif","bmp","png");
	// 对pimgs执行上传操作
	$file_name = "pimgs";
	$dir = "./uploads/pimgs";
	$arr_imgs = _uploads($file_name, $types, $dir, $arr_pimgs);
	if (is_array($arr_imgs)){
		$pimgs = implode(";",$arr_imgs);
	}
	// 对timgs执行上传操作
	$file_name = "timgs";
	$dir = "./uploads/timgs";
	$arr_imgs = _uploads($file_name, $types, $dir, $arr_timgs);
	if (is_array($arr_imgs)){
		$timgs = implode(";",$arr_imgs);
	}

	$colors = implode(";",$color);
	if (is_array($size)){
		$sizes = implode(";",$size);
	}
	$pubtime = time();
	// 链接图片描述
	$timg_titles = implode(';',$timg_descp);
	$pimg_titles = implode(';',$pimg_descp);

	$sql = "insert into products set p_c_id=$p_c_id, pname='$pname', price=$price, timgs='$timgs', descp='$descp', pubtime=$pubtime, pubadmin='$pubadmin', addr='$addr', color='$colors', size='$sizes', ifshow=$ifshow, keywords='$keywords', series='$series', pimgs='$pimgs', pimg_titles='$pimg_titles', timg_titles='$timg_titles'";
	
	// 如果设置了sprice,num_stock,num_click，则将其加入sql语句
	$sql .= $sprice!=''?", sprice=$sprice":"";
	$sql .= $num_stock!=''?", num_stock=$num_stock":"";
	$sql .= $num_click!=''?", num_click=$num_click":"";
	mysql_query($sql);
	if (mysql_affected_rows()==1){
		msg("添加成功","product_add.php"); die;
	}else{
		msg("添加失败，请重试","product_add.php"); die;
	}

	}
?>
<!doctype html>
<html>
	<head>
		<title> New Document </title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<link rel="stylesheet" href="skin/css/base.css" />
		<link rel="stylesheet" href="skin/css/main.css" />
		<link rel="stylesheet" href="skin/css/main1.css" />
	</head>

	<body>
		<table class="outer_tab">
			<tr>
				<td class="title" id="tabtitle" colspan=2><span><img src='skin/images/frame/arr3.gif'>添加商品：</span></td>
			</tr>
			<tr class="tr2">
				<td>
		<table class="inner_tab">
			<form method="post" action="#" enctype="multipart/form-data">
				<input type="hidden" name="pubadmin" value="<?=$_SESSION['admin']?>" />
				<tr>
					<td colspan=2>****请先确定<font color="red">颜色</font>，<font color='red'>尺码</font>和<font color="red">图片库</font>的条目数量，点击对应的“<font color='blue'>添加条目</font>”即可增加条目数量****</td>
				</tr>
				<tr>
					<td class='tdleft'><span class="tdtitle">*商品名称：</span></td>
					<td><input type="text" name="pname" class="input_textarea" /></td>
				</tr>
				<tr>
					<td class='tdleft'><span class="tdtitle">*商品价格：</span></td>
					<td><input type="text" name="price" class="input_textarea" /></td>
				</tr>
				<tr>
					<td class='tdleft'><span class="tdtitle">*产地：</span></td>
					<td><input type="text" name="addr" class="input_textarea" /></td>
				</tr>
				<tr>
					<td><span class="tdtitle">*商品分类：</span></td>
					<td>
						<select name='p_c_id'>
							<option value="0" />顶级分类</option>
							<?php 
								$sql = "select * from product_cate";
								$re = mysql_query($sql);
								$rec = array();
								while ($rec = mysql_fetch_assoc($re)){
									$arr[] = $rec;
								}
								function get_cates($arr,$value=0){
									static $classes = array();
									foreach ($arr as $key=>$rec){
										if ($rec['fid']==$value){
											$classes[] = $rec;
											get_cates($arr,$rec['cid']);
										}
									}
									return $classes;
								}
								$sorted_arr = get_cates($arr);
								foreach ($sorted_arr as $rec){
							?>
							<option value="<?=$rec['cid']?>" <?php 
								if (isset($record['p_c_id'])&&($record['p_c_id']==$rec['cid']))
									echo "selected";
							?>>
								<?=str_repeat("----",$rec['level']-1).$rec['cname']?>
							</option>
							<?php }?>
						</select>
					</td>
				</tr>
				<tr>
					<td><span class="tdtitle">产品序列号：</span></td>
					<td><input type="text" name="series" class="input_textarea" /></td>
				</tr>
				<tr>
					<td><span class="tdtitle">产品描述：</span></td>
					<td>
					
<?php		
	// 富文本编辑器
	include_once("./fckeditor/fckeditor.php");
	$fckeditor = new FCKeditor("descp");//定义默认值 name
	$fckeditor->Width = "400px";//定义编辑器的宽度
	$fckeditor->Height = "100px";//定义编辑器的高度
	$fckeditor->Value = $record['descp'];//定义默认值
	$fckeditor->BasePath='./fckeditor/';
	$fckeditor->ToolbarSet = "Basic";
	$fckeditor->Create();//创建编辑器
?>
					</td>
				</tr>
				<tr>
					<td><span class="tdtitle">颜色：</span></td>
					<td>
						<div title="red" alt="red" style="float:left;width:20px;height:15px;background:red"></div><input type="checkbox" name="color[]" value="red" style="float:left;margin-right:15px" />
						<div title="blue" alt="blue" style="float:left;width:20px;height:15px;background:blue"></div><input type="checkbox" name="color[]" value="blue" style="float:left;margin-right:15px" />
						<div title="black" alt="black" style="float:left;width:20px;height:15px;background:black"></div><input type="checkbox" name="color[]" value="black" style="float:left;margin-right:15px" checked />
						<div title="white" alt="white" style="float:left;width:19px;height:14px;background:white;border:1px solid black"></div><input type="checkbox" name="color[]" value="white" style="float:left;margin-right:15px" checked />
						<?php for ($i=0;$i<$num_color;$i++){?>
						<input type="text" name='color[]' />
						<?php }?>
						<!--添加条目（颜色）-->
						<a href="product_add.php?num_color=<?=$num_color+1?>&num_size=<?=$num_size?>&num_pimgs=<?=$num_pimgs?>"><font color='blue'>添加条目</font></a>
					</td>
				</tr>
				<tr>
					<td><span class="tdtitle">尺码：</td>
					<td>
	<span style="float:left;font-weight:bold;margin-right:5px">小<input type="checkbox" name="size[]" value='small'style="margin-right:15px" /></span>
	<span style="float:left;font-weight:bold;margin-right:5px">中<input type="checkbox" name="size[]" value='medium'style="margin-right:15px" /></span>
	<span style="float:left;font-weight:bold;margin-right:5px">大<input type="checkbox" name="size[]" value='big'style="margin-right:15px" /></span>
						<?php for ($i=0;$i<$num_size;$i++){?>
						<input type="text" name='size[]' />
						<?php }?>
						<!-- 添加条目（尺码）-->
						<a href="product_add.php?num_size=<?=$num_size+1?>&num_color=<?=$num_color?>&num_pimgs=<?=$num_pimgs?>"><font color="blue">添加条目</font></a>
					</td>
				</tr>
				<tr>
					<td><span class="tdtitle">优惠价：</span></td>
					<td><input type="text" name="sprice" class="input_textarea" /></td>
				</tr>
				<tr>
					<td><span class="tdtitle">关键词：</span></td>
					<td><input type="text" name="keywords" class="input_textarea" /></td>
				</tr>
				<tr>
					<td><span class="tdtitle">库存：</span></td>
					<td><input type="text" name="num_stock" class="input_textarea" /></td>
				</tr>
				<tr>
					<td><span class="tdtitle">点击量：</span></td>
					<td><input type="text" name="num_click" class="input_textarea" /></td>
				</tr>
				<tr>
					<td><span class="tdtitle">上架/下架：</span></td>
					<td>
						<input type="radio" name="ifshow" value="1" checked/>上架
						<input type="radio" name="ifshow" value="0" />下架
					</td>
				</tr>
				<tr>
					<td><span class="tdtitle">标题图片：</span></td>
					<td>					
						<input type="hidden" name="MAX_FILE_SIZE" value="28000000" />
						<input type="text" name="timg_descp[]" class="input_textarea" />
						<input type="file" name="timgs[]" />
						<br/>
						<input type="text" name="timg_descp[]" class="input_textarea" />
						<input type="file" name="timgs[]" />
						<br/>
						<input type="text" name="timg_descp[]" class="input_textarea" />
						<input type="file" name="timgs[]" />
						<br/>
						<input type="text" name="timg_descp[]" class="input_textarea" />
						<input type="file" name="timgs[]" />
						<br/>
					</td>
				</tr>
				<tr>
					<td><span class="tdtitle">图片库：</span></td>
					<td>
						<input type="text" name="pimg_descp[]" class="input_textarea" />
						<input type="file" name="pimgs[]" /><br/>
						<input type="text" name="pimg_descp[]" class="input_textarea" />
						<input type="file" name="pimgs[]" /><br/>
						<input type="text" name="pimg_descp[]" class="input_textarea" />
						<input type="file" name="pimgs[]" /><br/>
						<input type="text" name="pimg_descp[]" class="input_textarea" />
						<input type="file" name="pimgs[]" /><br/>
						<?php for ($i=4;$i<$num_pimgs;$i++){?>
						<input type="text" name="pimg_descp[]" class="input_textarea" />
						<input type="file" name="pimgs[]" /><br/>
						<?php }?>
						<!-- 添加条目（图片） -->
						<a href="product_add.php?num_color=<?=$num_color?>&num_size=<?=$num_size?>&num_pimgs=<?=$num_pimgs+1?>"><font color="blue">添加条目</font></a>
					</td>
				</tr>
				<tr>
					<td colspan=2 class="operation">
						<input type="submit" class="coolbt2" name="sub" value="添加">
					</td>
				</tr>
			</form>
		</table>
				</td>
			</tr>
		</table>
	</body>
</html>
