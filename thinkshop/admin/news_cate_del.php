<?php
	include_once("global.php");
	if (!isset($_SESSION['admin'])){
		msg("请先登录","index.php"); die;
	}
	if (!isset($_GET['cid'])){
		msg('请通过新闻分类管理页面删除',"product_cate_list.php");
		die;
	}
	$cid = $_GET['cid'];
	$sql = "select * from news_cate where fid=$cid";
	$re = mysql_query($sql);
	if (mysql_fetch_assoc($re)>0){
		msg('该分类下有子分类或新闻，无法删除','news_cate_list.php');
		die;
	}
	$sql = "delete from news_cate where cid=$cid";
	mysql_query($sql);
	if (mysql_affected_rows()<1){
		msg('删除失败','news_cate_list.php');
		die;
	}else{
		msg('删除成功','news_cate_list.php');
		die;
	}
?>
