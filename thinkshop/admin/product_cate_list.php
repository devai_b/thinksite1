<?php
	include_once("global.php");
	if (!isset($_SESSION['admin'])){
		msg("请先登录","index.php"); die;
	}
	$sql = "select * from product_cate";
	$re = mysql_query($sql);
	if (mysql_num_rows($re)<1){
		msg("数据库里没有任何数据","product_cate_add.php");
		die;
	}
	while ($rec = mysql_fetch_assoc($re)){
		$arr[] = $rec;
	}
	function get_cates($arr,$value=0){
		static $classes = array();
		foreach ($arr as $key=>$rec){
			if ($rec['fid']==$value){
				$classes[] = $rec;
				get_cates($arr,$rec['cid']);
			}
		}
		return $classes;
	}
	$sorted_arr = get_cates($arr);
?>
<!doctype html>
<html>
	<head>
		<title> 商品分类管理 </title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<link rel="stylesheet" href="./skin/css/base.css" />
		<link rel="stylesheet" href="./skin/css/main.css" />
		<link rel="stylesheet" href="./skin/css/main1.css" />
	</head>

	<body>
		<table class="outer_tab">
		<tr>
			<td class="title" id="tabtitle"><span><img src='skin/images/frame/arr3.gif'>商品分类管理</span></td>
		</tr>
		<tr class="tr2"><td>
		<table class="inner_tab1">
			<tr class="inner_tab1_head">
				<td width="8%">编号</td>
				<td width="20%">分类名</td>
				<td width="10%">添加子类</td>
				<td width="10%">修改 / 删除</td>
			</tr>
			<?php foreach ($sorted_arr as $rec){?>
			<tr class="inner_tab1_content">
				<td><?=$rec['cid']?></td>
				<td class="catename"><span><?=str_repeat("----",$rec['level']-1).$rec['cname']?></span></td>
				<td><a href="product_cate_add.php?cid=<?=$rec['cid']?>"><img class="buttons" src='./skin/images/frame/menu-ex.png' title="添加" alt="添加" /></a>
				</td>
				<td>
					<a href="product_cate_edit.php?cid=<?=$rec['cid']?>"><img class="buttons" src='./skin/images/frame/trun.gif' title="修改" alt="修改" /></a>&nbsp;/&nbsp;
					<a href="product_cate_del.php?cid=<?=$rec['cid']?>"><img class="buttons" src='./skin/images/frame/gtk-del.png' title="删除" alt="删除" /></a>
				</td>
			</tr>
			<?php }?>
		</table>
	</td></tr>
	<tr>
		<td><input class="coolbg np" type="button" value="添加商品分类" onclick="location='product_cate_add.php';"><td>
	</tr>
	</table>
	</body>
</html>
