<?php
	include_once("global.php");
	// 管理员是否登录
	if (!isset($_SESSION['admin'])){
		msg("请先登录","ad_login.php"); die;
	}
	
	if (!isset($_GET['nid']) && !isset($_POST['nid'])){
		msg("请通过新闻管理页面修改","news_list.php"); die;
	}elseif (isset($_GET['nid'])){
		$nid = $_GET['nid'];
	}else{
		$nid = $_POST['nid'];
	}
	// 图片库项目的条数
	if (!isset($_GET['num_pimgs'])){
		$num_pimgs= 4;
	}else{
		$num_pimgs = $_GET['num_pimgs'];
	}
	if (isset($_POST['sub'])){
		$pubadmin = $_SESSION['admin'];
		$title = trim($_POST['title']);
		$content = trim($_POST['content']);
		$n_c_id = $_POST['n_c_id'];
		$num_click = $_POST['num_click'];
		$ifshow = $_POST['ifshow'];
		$timg_descp = $_POST['timg_descp'];
		$pimg_descp = $_POST['pimg_descp'];
		$arr_pimgs = $_POST['pimgs'];
		$arr_timgs = $_POST['timgs'];
		$pimgs_mix = $_POST['pimgs_mix'];
		$timgs_mix = $_POST['timgs_mix'];
		if (!isset($title) || !isset($n_c_id) || !isset($content) || $title=='' || $n_c_id=='' || $content==''){
			msg("带*的为必填内容，请填写完整","news_list.php"); die;
		}
		// 判断是否是在终极分类下添加
		$sql = "select * from news_cate where fid=$n_c_id";
		$re = mysql_query($sql);
		if (mysql_num_rows($re)>0){
			msg("该分类下还有子分类，只能在终极分类下添加商品","news_list.php"); die;
		}
	 
	// 图片上传函数	
	function _uploads($file_name, $types, $dir, $arr_imgs){
		if (!isset($_POST["sub"]))
			return "未通过post提交<br/>";

		$file = $_FILES[$file_name];
		$name = $file["name"];
		$error = $file['error'];
		$tmp_name = $file['tmp_name'];
		// echo ini_get("post_max_size")."<br/>";
		// echo ini_get("max_file_uploads")."<br/>";

		$str = implode("",$name);
		if ($str=="")
			return $arr_imgs;
		
		if (!is_dir($dir))
			mkdir($dir);

		for ($i=0; $i<count($name); $i++){
			if ($error[$i]!=0){
				echo "第".$i."个上传文件出错<br/>";
				continue;
			}

			$type = strtolower(end(explode(".",$name[$i])));


			if (!in_array($type, $types)){
				echo "第".$i."个文件类型不符合<br/>";
				continue;
			}

			if (!is_uploaded_file($tmp_name[$i])){
				echo "第".$i."个文件的临时文件不存在<br/>";
				continue;
			}
			
			if (substr($dir,-1)!='/')
				$dir .= '/';
			// $path = $dir.time()."_".rand(1,1000).".".$type;
			$path = $dir.$name[$i];
			// 图片重复判断（根据上传的文件名）
			if (is_array($arr_imgs)){
				if (in_array($path,$arr_imgs)){
					$msg = "第".($i+1)."个文件名重复，该图片可能已经上传";
					echo "<script>alert('$msg');</script>";
					continue;
			}}
			if (move_uploaded_file($tmp_name[$i], $path)){
				$arr_imgs[] = $path;
				echo "第".$i."个文件上传成功<br/>";
			}
			else
				echo "第".$i."个文件上传失败<br/>";
		}
		return $arr_imgs;
	}

	// 链接图片描述
	if (isset($timgs_mix) && $timgs_mix[0]!=''){
		$timg_descp1 = $timg_descp;
		$timg_descp = array();
		foreach ($timgs_mix as $timg_mix){
			$mix0 = explode('&',$timg_mix);
			$arr_timgs[] = $mix0[0];
			$timg_descp[] = $mix0[1];
		}
		foreach ($timg_descp1 as $t){
			$timg_descp[] = $t;
		}
	}
	if (isset($pimgs_mix) && $pimgs_mix[0]!=''){
		$pimg_descp1 = $pimg_descp;
		$pimg_descp = array();
		foreach ($pimgs_mix as $pimg_mix){
			$mix1 = explode('&',$pimg_mix);
			$arr_pimgs[] = $mix1[0];
			$pimg_descp[] = $mix1[1];
		}
		foreach ($pimg_descp1 as $p){
			$pimg_descp[] = $p;
		}
	}
	$timg_titles = trim(implode(';',$timg_descp),';');
	$pimg_titles = trim(implode(';',$pimg_descp),';');
	
	$types = array("jpg","jpeg","gif","bmp","png");
	// 对pimgs执行上传操作
	$file_name = "pimgs";
	$dir = "./uploads/pimgs";
	$arr_imgs = _uploads($file_name, $types, $dir, $arr_pimgs);
	if (is_array($arr_imgs)){
		$pimgs = trim(implode(";",$arr_imgs),';');
	}
	// 对timgs执行上传操作
	$file_name = "timgs";
	$dir = "./uploads/timgs";
	$arr_imgs = _uploads($file_name, $types, $dir, $arr_timgs);
	if (is_array($arr_imgs)){
		$timgs = trim(implode(";",$arr_imgs),';');
	}
	$pubtime = time();

	$sql = "update news set n_c_id=$n_c_id, title='$title', content='$content', timgs='$timgs', pubtime=$pubtime, pubadmin='$pubadmin', ifshow=$ifshow, keywords='$keywords', pimgs='$pimgs', pimg_titles='$pimg_titles', timg_titles='$timg_titles'";
	echo $sql;
	
	// 如果设置了num_click，则将其加入sql语句
	$sql .= $num_click!=''?", num_click=$num_click":"";
	$sql .= " where nid=$nid";
	mysql_query($sql);
	$toPage = "news_edit.php?nid=".$nid;
	if (mysql_affected_rows()==1){
		msg("修改成功",$toPage); die;
	}else{
		msg("修改失败，请重试",$toPage); die;
	}

	}
	$sql = "select * from news where nid=$nid";
	$re = mysql_query($sql);
	if (mysql_num_rows($re)!=1){
		msg("数据库中没有对应的数据","news_list.php"); die;
	}
	$record = mysql_fetch_assoc($re);
?>
<!doctype html>
<html>
	<head>
		<title> New Document </title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<link rel="stylesheet" href="skin/css/base.css" />
		<link rel="stylesheet" href="skin/css/main.css" />
		<link rel="stylesheet" href="skin/css/main1.css" />
	</head>

	<body>
		<table class="outer_tab">
			<tr>
				<td class="title" id="tabtitle" colspan=2><span><img src='skin/images/frame/arr3.gif'>添加新闻：</span></td>
			</tr>
			<tr class="tr2">
				<td>
		<table class="inner_tab">
			<form method="post" action="#" enctype="multipart/form-data">
				<input type="hidden" name="pubadmin" value="<?=$_SESSION['admin']?>" />
				<tr>
					<td colspan=2>*****请先确定<font color="red">图片库</font>的条目数量，点击对应的“<font color='blue'>添加条目</font>”即可增加条目数量*****</td>
				</tr>
				<tr>
					<td class='tdleft'><span class="tdtitle">*新闻名称：</span></td>
					<td><input type="text" name="title" class="input_textarea" value="<?=$record['title']?>" /></td>
				</tr>
				<tr>
					<td><span class="tdtitle">*新闻分类：</span></td>
					<td>
						<select name='n_c_id'>
							<option value="0" />顶级分类</option>
							<?php 
								$sql = "select * from news_cate";
								$re = mysql_query($sql);
								$rec = array();
								while ($rec = mysql_fetch_assoc($re)){
									$arr[] = $rec;
								}
								function get_cates($arr,$value=0){
									static $classes = array();
									foreach ($arr as $key=>$rec){
										if ($rec['fid']==$value){
											$classes[] = $rec;
											get_cates($arr,$rec['cid']);
										}
									}
									return $classes;
								}
								$sorted_arr = get_cates($arr);
								foreach ($sorted_arr as $rec){
							?>
							<option value="<?=$rec['cid']?>" <?php 
								if (isset($record['n_c_id'])&&($record['n_c_id']==$rec['cid']))
									echo "selected";
							?>>
								<?=str_repeat("----",$rec['level']-1).$rec['cname']?>
							</option>
							<?php }?>
						</select>
					</td>
				</tr>
				<tr>
					<td><span class="tdtitle">*新闻内容：</span></td>
					<td>
					
<?php		
	// 富文本编辑器
	include_once("./fckeditor/fckeditor.php");
	$fckeditor = new FCKeditor("content");//定义默认值 name
	$fckeditor->Width = "800px";//定义编辑器的宽度
	$fckeditor->Height = "300px";//定义编辑器的高度
	$fckeditor->Value = $record["content"]; //定义默认值
	$fckeditor->BasePath='./fckeditor/';
	$fckeditor->ToolbarSet = "Basic";
	$fckeditor->Create();//创建编辑器
?>
					</td>
				</tr>
				<tr>
					<td><span class="tdtitle">关键词：</span></td>
					<td><input type="text" name="keywords" value="<?=$record['keywords']?>"class="input_textarea" /></td>
				</tr>
				<tr>
					<td><span class="tdtitle">点击量：</span></td>
					<td><input type="text" name="num_click" value="<?=$record['num_click']?>" class="input_textarea" /></td>
				</tr>
				<tr>
					<td><span class="tdtitle">显示/不显示：</span></td>
					<td>
						<input type="radio" name="ifshow" value="1" <?=$record['ifshow']==1?"checked":""?> />显示
						<input type="radio" name="ifshow" value="0" <?=$record['ifshow']==0?"checked":""?> />不显示
					</td>
				</tr>
				<tr>
					<td><span class="tdtitle">标题图片：</span></td>
					<td>					
						<input type="hidden" name="MAX_FILE_SIZE" value="28000000" />
<?php
			$timg_descps = explode(";",trim($record['timg_titles'],';'));
			echo "图片预览：";
			$imgs = explode(";",trim($record['timgs'],';'));
			$i = 0;
?>
<?php 
						
						foreach ($imgs as $img){
							if (isset($img) && $img!=''){
?>
						<input type="checkbox" name="timgs_mix[]" value="<?=$img?>&<?=$timg_descps[$i]?>" checked />
						<img src='<?=$img?>' height="50px" title="<?=$timg_descps[$i]?>" />
	<?php 
						$i++;
		}}?><br/>
						<input type="text" name="timg_descp[]" class="input_textarea" />
						<input type="file" name="timgs[]" />
						<br/>
						<input type="text" name="timg_descp[]" class="input_textarea" />
						<input type="file" name="timgs[]" />
						<br/>
						<input type="text" name="timg_descp[]" class="input_textarea" />
						<input type="file" name="timgs[]" />
						<br/>
						<input type="text" name="timg_descp[]" class="input_textarea" />
						<input type="file" name="timgs[]" />
						<br/>
					</td>
				</tr>
				<tr>
					<td><span class="tdtitle">图片库：</span></td>
					<td>

				<?php
				$pimg_descps = explode(";",trim($record['pimg_titles'],';'));
				echo "图片预览：";
				$imgs = explode(";",trim($record['pimgs'],';'));
				$num_imgs = count($imgs);
				if ($imgs!=array() && $imgs[0]!=""){
						$i = 0;
						foreach ($imgs as $img){
?>
						<input type="checkbox" name="pimgs_mix[]" value="<?=$img?>&<?=$pimg_descps[$i]?>" checked />
						<img src="<?=$img?>" height='50px' title=<?=$pimg_descps[$i]?> />
						<?php 
							$i++;
						}}?>
						<br/>
						<input type="text" name="pimg_descp[]" class="input_textarea" />
						<input type="file" name="pimgs[]" /><br/>
						<input type="text" name="pimg_descp[]" class="input_textarea" />
						<input type="file" name="pimgs[]" /><br/>
						<input type="text" name="pimg_descp[]" class="input_textarea" />
						<input type="file" name="pimgs[]" /><br/>
						<input type="text" name="pimg_descp[]" class="input_textarea" />
						<input type="file" name="pimgs[]" /><br/>
						<?php for ($i=4;$i<$num_pimgs;$i++){?>
						<input type="text" name="pimg_descp[]" class="input_textarea" />
						<input type="file" name="pimgs[]" /><br/>
						<?php }?>
						<!-- 添加条目（图片） -->
						<a href="news_list.php?num_pimgs=<?=$num_pimgs+1?>"><font color="blue">添加条目</font></a>
					</td>
				</tr>
				<tr>
					<td colspan=2 class="operation">
						<input type="submit" class="coolbt2" name="sub" value="修改">
					</td>
				</tr>
			</form>
		</table>
				</td>
			</tr>
		</table>
	</body>
</html>
