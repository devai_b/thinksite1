<?php
	include_once("global.php");
	if (!isset($_SESSION['admin'])){
		msg("请先登录","index.php"); die;
	}else{
		// unset($_SESSION['admin']);
		$_SESSION = array();
		session_destroy();
		if (isset($_SESSION['admin'])){
			msg("退出失败","index.php"); die;
		}else{
			msg("退出成功","index.php"); die;
		}
	}
?>
