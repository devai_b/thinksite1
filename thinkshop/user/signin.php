<?php
	include_once("./head.html");
?>
<!doctype html>
<html>
	<head>
		<title>注册页面</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8;">
		<link rel="stylesheet" href="./css/signup.css">
	</head>
	<body>
		<div id="up">
			<p>注册欣才商城账号</p>
		</div>
		<div id="down">
			<div id="left">
				<div class="logo">
					<p class="thinksite_en">Thinksite</p>
					<p class="thinksite_ch">欣才商城</p>
				</div>
			</div>
			<div id="right">
				<div class="right1">
					<div class="input_item">
						<p>账户</p>
					</div>
					<div class="input_content">
						<input type="text" name="acc" class="input_long" id="account" />
					</div>
					<div class="input_item">
						<p>密码</p>
					</div>
					<div class="input_content">
						<input type="text" name="password" class="input_long" id="password" />
					</div>
				</div>
				<div class="right3">
					<input type="submit" id="signin" name="add" value="登陆" />
				</div>
				<div></div>
			</div>
		</div>
	</body>
</html>
<?php
	include_once("./bottom.html");
?>