<?php
	require_once("./head.html");
?>
<?php
	$arr = array(
			array("name"=>"初级课程c01","price"=>"119.00"),
			array("name"=>"初级课程c02","price"=>"380.00"),
			array("name"=>"初级课程c03","price"=>"150.00"),
			array("name"=>"初级课程c04","price"=>"135.00"),
			array("name"=>"初级课程c05","price"=>"180.00"),
			array("name"=>"初级课程c06","price"=>"240.00"),
			array("name"=>"初级课程c07","price"=>"480.00"),
			array("name"=>"初级课程c08","price"=>"190.00"),
			array("name"=>"初级课程c09","price"=>"119.00"),
			array("name"=>"初级课程c10","price"=>"125.00"),
			array("name"=>"初级课程c11","price"=>"140.00"),
			array("name"=>"初级课程c12","price"=>"195.00"),
			)
?>
<!doctype html>
<html>
	<head>
		<title> New Document </title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<link rel="stylesheet" href="./css/index.css">
	</head>

	<body>
		<div id="top">
			<ul>
				<li>分&nbsp;&nbsp;&nbsp;&nbsp;类：</li>
				<li><a name="all" href="#" target="_self">全部</a></li>
				<li><a name="fundamental" href="#" target="_self">初级课程</a></li>
				<li><a name="intermediate" href="#" target="_self"><span class="highlight">中级课程<span></a></li>
				<li><a name="advanced" href="#" target="_self">高级课程</a></li>
				<li><a name="practice" href="#" target="_self">实战知识</a></li>
				<li><a name="others" href="#" target="_self">其他</a></li>
			</ul>
			<span id="date"></span>
			<script>
				var date = document.getElementById("date");
				var d = new Date;
				date.innerHTML = d;
			</script>
		</div>
		<div id="middle">
			<?php
				foreach ($arr as $course){
			?>
			<div class="course">
				<p class="thinksite">Thinksite</p>
				<p class="classroom">Classroom</p>
				<p class="course_name"><?=$course["name"] ?></p>
				<p class="price">￥<?=$course["price"]?></p>
				<input type="submit" name="buy" class="buy" value="立即购买" />
			</div>
			<?php } ?>
			<div class="pages">
				<div class="page_current">1</div>
				<div class="page_following"><a name="page_2" href="#">2</a></div>
				<div class="nextpage"><a name="page_next" href="#">下一页</a></div>
			</div>
		</div>
		<div id="bottom">
			<div class="guarantees">
				<ul>
					<li><span class="highlight">7</span>日免费退货</li>
					<li><span class="highlight">15</span>日免费更换</li>
					<li><span class="highlight">全场</span>包邮</li>
					<li><span class="highlight">全国</span>范围均可配送</li>
				</ul>
			</div>
			<div class="tips">
				<ul>
					<li><a name="pay" href="#">支付方式</a></li>
					<li>|</li>
					<li><a name="distribution" href="#">配送说明</a></li>
					<li>|</li>
					<li><a name="services" href="#">售后服务</a></li>
				</ul>
			</div>
		</div>
	</body>
</html>
<?php
	require_once("./bottom.html");
?>
