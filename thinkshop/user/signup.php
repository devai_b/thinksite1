<?php
	include_once("./head.html");
?>
<!doctype html>
<html>
	<head>
		<title>注册页面</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8;">
		<link rel="stylesheet" href="./css/signup.css">
	</head>
	<body>
		<div id="up">
			<p>注册欣才商城账号</p>
		</div>
		<div id="down">
			<div id="left">
				<div class="logo">
					<p class="thinksite_en">Thinksite</p>
					<p class="thinksite_ch">欣才商城</p>
				</div>
				<p class="p1">知其然知其所以然！</p>
				<p class="p2">通过我们的实战项目，您可以更全面的理解软件工程</p>
			</div>
			<div id="right">
				<div class="right0">
					<div class="input_item">
						<p>账户</p>
					</div>
					<div class="input_content">
						<input type="text" name="account" class="input_long" id="account" />
					</div>
				</div>
				<div class="right1">
					<div class="input_item">
						<p>密码</p>
					</div>
					<div class="input_content">
						<input type="text" name="password" class="input_long" id="password" />
					</div>
					<div class="input_item">
						<p>确认密码</p>
					</div>
					<div class="input_content">
						<input type="text" name="password_again" class="input_long" id="password_again" />
					</div>
				</div>
				<div class="right2">
					<div class="input_item">
						<p>昵称</p>
					</div>
					<div class="input_content">
						<input type="text" name="nickname" class="input_long" id="nickname" />
					</div>
					<div class="input_item">
						<p>邮箱</p>
					</div>
					<div class="input_content">
						<input type="text" name="email" class="input_long" id="email" />
					</div>
					<div class="input_item">
						<p>验证码</p>
					</div>
					<div class="input_content">
						<input type="text" name="captcha" class="input_short" id="captcha" />
						<div id="img_captcha"></div>
					</div>
					<div id="ifaccept">
						<input type="checkbox" name="acc[]" id="accept" value="1" checked />
						<p>接受 <a id="terms" href="#" target="_blank">欣才商城服务条款</a></p>
					</div>
				</div>
				<div class="right3">
					<input type="submit" id="signup" name="add" value="注册" />
				</div>
				<div></div>
			</div>
		</div>
	</body>
</html>
<?php
	include_once("./bottom.html");
?>